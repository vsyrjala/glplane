/*
 * Copyright (C) 2012-2013 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <assert.h>
#include <errno.h>
#include <math.h>
#include <errno.h>
#include <fcntl.h>
#include <time.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <termios.h>

#include <time.h>

#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>

#include <xf86drm.h>
#include <xf86drmMode.h>
#include <drm_fourcc.h>
#include <gbm.h>

#include "utils.h"
#include "gutils.h"
#include "term.h"
#include "common.h"
#include "gl.h"

//#define dprintf printf
#define dprintf(x...) do {} while (0)

#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

#define DRM_ROTATE_0 0
#define DRM_ROTATE_180 2

static enum {
	ANIM_CURVE,
	ANIM_RAND,
	ANIM_STATIC,
	ANIM_COUNT,
} anim_mode;

static bool anim_clear;
static bool toggle_onoff;
static unsigned int setplane_delay;

struct region {
	int32_t x1;
	int32_t y1;
	int32_t x2;
	int32_t y2;
};


struct my_crtc {
	struct crtc base;

	bool dirty;
	bool dirty_mode;
	bool dirty_dpms;

	unsigned int dispw;
	unsigned int disph;

	struct my_surface surf;
	struct buffer *buf;

	uint32_t original_fb_id;
	uint32_t fb_id;
#ifdef LEGACY_API
	uint32_t old_fb_id;
#endif

	drmModeModeInfo original_mode;
	drmModeModeInfo mode[2];
	int mode_idx;
	bool dpms;

	unsigned int frames;
	struct timespec prev;

	struct {
		uint32_t src_x;
		uint32_t src_y;
		uint32_t fb;
		uint32_t mode;
		uint32_t connector_ids;
		uint32_t cursor_id;
		uint32_t cursor_x;
		uint32_t cursor_y;
		uint32_t cursor_w;
		uint32_t cursor_h;
		uint32_t dpms;
	} prop;

	uint32_t connector_ids[8];

	struct {
		bool dirty;
		uint32_t id;
		struct bo bo;
	} cursor;

	bool onoff_phase;
	unsigned int toggle_onoff_cntr;
};

struct my_plane {
	struct plane base;

	bool dirty;

	struct my_surface surf;
	struct buffer *buf;

	struct region src; /* 16.16 */
	struct region dst;

	bool enable;
	uint32_t fb_id;
#ifdef LEGACY_API
	uint32_t old_fb_id;
#endif
	bool rotate;

	struct {
		uint32_t src_x;
		uint32_t src_y;
		uint32_t src_w;
		uint32_t src_h;

		uint32_t crtc_x;
		uint32_t crtc_y;
		uint32_t crtc_w;
		uint32_t crtc_h;

		uint32_t fb;
		uint32_t crtc;
		uint32_t rotation;
	} prop;

	struct {
		float ang;
		float rad_dir;
		float rad;
		int w_dir;
		int w;
		int h_dir;
		int h;
	} state;
};

struct my_ctx {
	int fd;
	int count_crtcs;
	struct my_plane *planes;
#ifndef LEGACY_API
	drmModePropertySetPtr set;
	uint32_t flags;
#endif
};

static bool throttle;
static bool blur;
static bool blank;
static bool render = true;

static int get_free_buffer(struct my_surface *surf)
{
	if (throttle && surf->pending_events > 0)
		return -1;
	if (surface_has_free_buffers(&surf->base))
		return 0;

	return -1;
}

#define ALIGN(x, a) (((x) + (a) - 1) & ~((a) - 1))

static void plane_enable(struct my_plane *p, bool enable)
{
	if (p->enable == enable)
		return;
	p->enable = enable;
	p->dirty = true;
	dprintf("plane %s\n", enable ? "enabled" : "disabled");
}

static void populate_crtc_props(int fd, struct my_crtc *c)
{
	drmModeObjectPropertiesPtr props;
	uint32_t i;

	props = drmModeObjectGetProperties(fd, c->base.crtc_id, DRM_MODE_OBJECT_CRTC);
	if (!props)
		return;

	for (i = 0; i < props->count_props; i++) {
		drmModePropertyPtr prop;

		prop = drmModeGetProperty(fd, props->props[i]);
		if (!prop)
			continue;

		printf("crtc prop %s %u\n", prop->name, prop->prop_id);

		if (!strcmp(prop->name, "SRC_X"))
			c->prop.src_x = prop->prop_id;
		else if (!strcmp(prop->name, "SRC_Y"))
			c->prop.src_y = prop->prop_id;
		else if (!strcmp(prop->name, "FB_ID"))
			c->prop.fb = prop->prop_id;
		else if (!strcmp(prop->name, "MODE"))
			c->prop.mode = prop->prop_id;
		else if (!strcmp(prop->name, "CONNECTOR_IDS"))
			c->prop.connector_ids = prop->prop_id;
		else if (!strcmp(prop->name, "CURSOR_ID"))
			c->prop.cursor_id = prop->prop_id;
		else if (!strcmp(prop->name, "CURSOR_X"))
			c->prop.cursor_x = prop->prop_id;
		else if (!strcmp(prop->name, "CURSOR_Y"))
			c->prop.cursor_y = prop->prop_id;
		else if (!strcmp(prop->name, "CURSOR_W"))
			c->prop.cursor_w = prop->prop_id;
		else if (!strcmp(prop->name, "CURSOR_H"))
			c->prop.cursor_h = prop->prop_id;

		drmModeFreeProperty(prop);
	}

	drmModeFreeObjectProperties(props);
}

static void populate_connector_props(int fd, struct my_crtc *c)
{
	drmModeObjectPropertiesPtr props;
	uint32_t i;

	props = drmModeObjectGetProperties(fd, c->base.connector_id, DRM_MODE_OBJECT_CONNECTOR);
	if (!props)
		return;

	for (i = 0; i < props->count_props; i++) {
		drmModePropertyPtr prop;

		prop = drmModeGetProperty(fd, props->props[i]);
		if (!prop)
			continue;

		printf("connector prop %s %u\n", prop->name, prop->prop_id);

		if (!strcmp(prop->name, "DPMS"))
			c->prop.dpms = prop->prop_id;

		drmModeFreeProperty(prop);
	}

	drmModeFreeObjectProperties(props);
}

static void populate_plane_props(int fd, struct my_plane *p)
{
	drmModeObjectPropertiesPtr props;
	uint32_t i;

	props = drmModeObjectGetProperties(fd, p->base.plane_id, DRM_MODE_OBJECT_PLANE);
	if (!props)
		return;

	for (i = 0; i < props->count_props; i++) {
		drmModePropertyPtr prop;

		prop = drmModeGetProperty(fd, props->props[i]);
		if (!prop)
			continue;

		printf("plane prop %s %u\n", prop->name, prop->prop_id);

		if (!strcmp(prop->name, "SRC_X"))
			p->prop.src_x = prop->prop_id;
		else if (!strcmp(prop->name, "SRC_Y"))
			p->prop.src_y = prop->prop_id;
		else if (!strcmp(prop->name, "SRC_W"))
			p->prop.src_w = prop->prop_id;
		else if (!strcmp(prop->name, "SRC_H"))
			p->prop.src_h = prop->prop_id;
		else if (!strcmp(prop->name, "CRTC_X"))
			p->prop.crtc_x = prop->prop_id;
		else if (!strcmp(prop->name, "CRTC_Y"))
			p->prop.crtc_y = prop->prop_id;
		else if (!strcmp(prop->name, "CRTC_W"))
			p->prop.crtc_w = prop->prop_id;
		else if (!strcmp(prop->name, "CRTC_H"))
			p->prop.crtc_h = prop->prop_id;
		else if (!strcmp(prop->name, "FB_ID"))
			p->prop.fb = prop->prop_id;
		else if (!strcmp(prop->name, "CRTC_ID"))
			p->prop.crtc = prop->prop_id;
		else if (!strcmp(prop->name, "rotation"))
			p->prop.rotation = prop->prop_id;

		drmModeFreeProperty(prop);
	}

	drmModeFreeObjectProperties(props);
}

static void atomic_event(int fd, unsigned int seq, unsigned int tv_sec, unsigned int tv_usec,
			 uint32_t obj_id, uint32_t old_fb_id, void *user_data)
{
	struct my_ctx *ctx = user_data;
	int i;

	for (i = 0; i < ctx->count_crtcs; i++) {
		struct my_plane *p = &ctx->planes[i];
		struct my_crtc *c = container_of(p->base.crtc, struct my_crtc, base);
		struct buffer *buf;

		dprintf("event: obj=%u old_fb=%u\n", obj_id, old_fb_id);

		if (obj_id == p->base.plane_id) {
			dprintf("plane pending events %u -> %u\n",
				p->surf.pending_events, p->surf.pending_events - 1);
			p->surf.pending_events--;
		} else if (obj_id == c->base.crtc_id) {
			dprintf("crtc pending events %u -> %u\n",
				c->surf.pending_events, c->surf.pending_events - 1);
			c->surf.pending_events--;
		} else
			continue;

		if (old_fb_id) {
			if (obj_id == p->base.plane_id) {
				buf = surface_find_buffer_by_fb_id(&p->surf.base, old_fb_id);
				if (buf)
					surface_buffer_put_fb(&p->surf.base, buf);
			} else if (obj_id == c->base.crtc_id) {
				buf = surface_find_buffer_by_fb_id(&c->surf.base, old_fb_id);
				if (buf)
					surface_buffer_put_fb(&c->surf.base, buf);
			} else
				dprintf("EVENT for unknown obj %u\n", obj_id);
		} else
			dprintf("EVENT w/o old_fb_id\n");

		break;
	}
}

#if 0
static void drmModePropertySetAdd2(drmModePropertySetPtr set, uint32_t obj_id, uint32_t prop_id, uint64_t value)
{
	printf("obj=%u, prop=%u, value=%llu\n", obj_id, prop_id, value);
	drmModePropertySetAdd(set, obj_id, prop_id, value);
}

static void drmModePropertySetAddBlob2(drmModePropertySetPtr set, uint32_t obj_id, uint32_t prop_id,
				       uint64_t length, void *blob)
{
	printf("obj=%u, prop=%u, length=%llu, blob=%p\n", obj_id, prop_id, length, blob);
	drmModePropertySetAddBlob(set, obj_id, prop_id, length, blob);
}

#define drmModePropertySetAdd drmModePropertySetAdd2
#define drmModePropertySetAddBlob drmModePropertySetAddBlob2
#endif

static void plane_commit(struct my_ctx *ctx, struct my_plane *p)
{
	struct my_crtc *c = container_of(p->base.crtc, struct my_crtc, base);
	int r;

	if (!p->dirty && !c->dirty && !c->dirty_mode && !c->dirty_dpms && !c->cursor.dirty)
		return;

	assert(c->buf == NULL);
	assert(p->buf == NULL);

#ifndef LEGACY_API
	if (!ctx->set) {
		ctx->set = drmModePropertySetAlloc();
		if (!ctx->set)
			return;
		ctx->flags = DRM_MODE_ATOMIC_EVENT | DRM_MODE_ATOMIC_NONBLOCK;
	}
#endif

#ifdef LEGACY_API
	/* need a buf to do SetCrtc call */
	if (c->dirty_mode)
		c->dirty = true;
#endif

	if (c->dirty) {
		c->buf = surface_get_front(ctx->fd, &c->surf.base);
		if (!c->buf)
			return;
	}

	if (p->dirty && p->enable) {
		p->buf = surface_get_front(ctx->fd, &p->surf.base);
		if (!p->buf) {
			if (c->buf) {
				surface_buffer_put_fb(&c->surf.base, c->buf);
				c->buf = NULL;
			}
			return;
		}
	}

#ifndef LEGACY_API
	if (c->dirty) {
		drmModePropertySetAdd(ctx->set,
				      c->base.crtc_id,
				      c->prop.fb,
				      c->buf->fb_id);

		drmModePropertySetAdd(ctx->set,
				      c->base.crtc_id,
				      c->prop.src_x,
				      0);

		drmModePropertySetAdd(ctx->set,
				      c->base.crtc_id,
				      c->prop.src_y,
				      0);

		dprintf("crtc pending events %u -> %u\n",
			c->surf.pending_events, c->surf.pending_events + 1);
		c->surf.pending_events++;
	}

	if (c->dirty_mode) {
		drmModePropertySetAddBlob(ctx->set,
					  c->base.crtc_id,
					  c->prop.mode,
					  sizeof(c->mode[0]),
					  &c->mode[c->mode_idx]);

		c->connector_ids[0] = c->base.connector_id;
		drmModePropertySetAddBlob(ctx->set,
					  c->base.crtc_id,
					  c->prop.connector_ids,
					  4,
					  c->connector_ids);

		/* don't try nonblocking modectx->set, the kernel will reject it. */
		ctx->flags &= ~DRM_MODE_ATOMIC_NONBLOCK;
	}

	if (c->cursor.dirty) {
		drmModePropertySetAdd(ctx->set,
				      c->base.crtc_id,
				      c->prop.cursor_id,
				      c->cursor.id);
		drmModePropertySetAdd(ctx->set,
				      c->base.crtc_id,
				      c->prop.cursor_x,
				      c->dispw/2);
		drmModePropertySetAdd(ctx->set,
				      c->base.crtc_id,
				      c->prop.cursor_y,
				      c->disph/2);
		drmModePropertySetAdd(ctx->set,
				      c->base.crtc_id,
				      c->prop.cursor_w,
				      64);
		drmModePropertySetAdd(ctx->set,
				      c->base.crtc_id,
				      c->prop.cursor_h,
				      64);
	}

	if (p->dirty) {
		drmModePropertySetAdd(ctx->set,
				      p->base.plane_id,
				      p->prop.fb,
				      p->buf ? p->buf->fb_id : 0);

		drmModePropertySetAdd(ctx->set,
				      p->base.plane_id,
				      p->prop.crtc,
				      p->base.crtc->crtc_id);

		drmModePropertySetAdd(ctx->set,
				      p->base.plane_id,
				      p->prop.src_x,
				      p->src.x1);

		drmModePropertySetAdd(ctx->set,
				      p->base.plane_id,
				      p->prop.src_y,
				      p->src.y1);

		drmModePropertySetAdd(ctx->set,
				      p->base.plane_id,
				      p->prop.src_w,
				      p->src.x2 - p->src.x1);

		drmModePropertySetAdd(ctx->set,
				      p->base.plane_id,
				      p->prop.src_h,
				      p->src.y2 - p->src.y1);

		drmModePropertySetAdd(ctx->set,
				      p->base.plane_id,
				      p->prop.crtc_x,
				      p->dst.x1);

		drmModePropertySetAdd(ctx->set,
				      p->base.plane_id,
				      p->prop.crtc_y,
				      p->dst.y1);

		drmModePropertySetAdd(ctx->set,
				      p->base.plane_id,
				      p->prop.crtc_w,
				      p->dst.x2 - p->dst.x1);

		drmModePropertySetAdd(ctx->set,
				      p->base.plane_id,
				      p->prop.crtc_h,
				      p->dst.y2 - p->dst.y1);

		dprintf("plane pending events %u -> %u\n",
			p->surf.pending_events, p->surf.pending_events + 1);
		p->surf.pending_events++;
	}
#else
	if (c->dirty || c->dirty_mode) {
		r = drmModeSetCrtc(ctx->fd, c->base.crtc_id, c->buf->fb_id,
				   0, 0, &c->base.connector_id, 1, &c->mode[c->mode_idx]);
		if (r)
			printf("drmModeSetCrtc() failed %d:%s\n", errno, strerror(errno));
	}

	if (c->cursor.dirty) {
		r = drmModeSetCursor(ctx->fd, c->base.crtc_id, c->cursor.id, 64, 64);
		if (r)
			printf("drmModeSetCursor() failed %d:%s\n", errno, strerror(errno));
		r = drmModeMoveCursor(ctx->fd, c->base.crtc_id, c->dispw/2, c->disph/2);
		if (r)
			printf("drmModeMoveCursor() failed %d:%s\n", errno, strerror(errno));
	}

	if (p->dirty) {
		if (setplane_delay)
			usleep(setplane_delay);

		if (p->prop.rotation) {
			r = drmModeObjectSetProperty(ctx->fd, p->base.plane_id,
						     DRM_MODE_OBJECT_PLANE,
						     p->prop.rotation,
						     p->rotate ? 1 << DRM_ROTATE_180 : 1 << DRM_ROTATE_0);
			if (r)
				printf("drmModeSetProperty() failed %d:%s\n", errno, strerror(errno));
		}
		r = drmModeSetPlane(ctx->fd, p->base.plane_id,
				p->base.crtc->crtc_id,
				p->buf ? p->buf->fb_id : 0,
				0,
				p->dst.x1, p->dst.y1,
				p->dst.x2 - p->dst.x1,
				p->dst.y2 - p->dst.y1,
				p->src.x1, p->src.y1,
				p->src.x2 - p->src.x1,
				p->src.y2 - p->src.y1);
		if (r)
			printf("drmModeSetPlane() failed %d:%s\n", errno, strerror(errno));
	}

	if (c->dirty_dpms) {
		r = drmModeConnectorSetProperty(ctx->fd, c->base.connector_id, c->prop.dpms,
						c->dpms ? DRM_MODE_DPMS_OFF : DRM_MODE_DPMS_ON);
		if (r)
			printf("drmModeConnectorSetProperty() failed %d:%s\n", errno, strerror(errno));
	}

#endif
}

static void tp_sub(struct timespec *tp, const struct timespec *tp2)
{
	tp->tv_sec -= tp2->tv_sec;
	tp->tv_nsec -= tp2->tv_nsec;
	if (tp->tv_nsec < 0) {
		tp->tv_nsec += 1000000000L;
		tp->tv_sec--;
	}
}

static void commit_state(struct my_ctx *ctx)
{
	struct timespec pre, post;
	int i, r;

#ifndef LEGACY_API
	if (!ctx->set)
		return;

	clock_gettime(CLOCK_MONOTONIC, &pre);
	//r = drmModePropertySetCommit(fd, DRM_MODE_ATOMIC_TEST_ONLY, set);
	r = drmModePropertySetCommit(ctx->fd, ctx->flags, ctx, ctx->set);
	clock_gettime(CLOCK_MONOTONIC, &post);
	tp_sub(&post, &pre);

	//printf("commit took %lu secs, %lu nsecs\n", post.tv_sec, post.tv_nsec);

	drmModePropertySetFree(ctx->set);
	ctx->set = NULL;

	if (r) {
		printf("setatomic returned %d:%s\n", errno, strerror(errno));

		for (i = 0; i < ctx->count_crtcs; i++) {
			struct my_plane *p = &ctx->planes[i];
			struct my_crtc *c = container_of(p->base.crtc, struct my_crtc, base);

			if (p->dirty) {
				unsigned int src_w = p->src.x2 - p->src.x1;
				unsigned int src_h = p->src.y2 - p->src.y1;
				unsigned int dst_w = p->dst.x2 - p->dst.x1;
				unsigned int dst_h = p->dst.y2 - p->dst.y1;

				printf("plane = %u, crtc = %u, fb = %u\n",
				       p->base.plane_id, p->base.crtc->crtc_id, p->buf ? p->buf->fb_id : 0);

				printf("src = %u.%06ux%u.%06u+%u.%06u+%u.%06u\n",
				       src_w >> 16, ((src_w & 0xffff) * 15625) >> 10,
				       src_h >> 16, ((src_h & 0xffff) * 15625) >> 10,
				       p->src.x1 >> 16, ((p->src.x1 & 0xffff) * 15625) >> 10,
				       p->src.y1 >> 16, ((p->src.y1 & 0xffff) * 15625) >> 10);

				printf("dst = %ux%u+%d+%d\n",
				       dst_w, dst_h, p->dst.x1, p->dst.y1);
			}

			if (c->dirty)
				printf("crtc = %u, fb = %u\n", c->base.crtc_id, c->buf ? c->buf->fb_id : 0);

			if (c->dirty_mode) {
				print_mode("mode", &c->mode[c->mode_idx]);

				printf("connector_id = %u\n", c->connector_ids[0]);
			}

			if (c->dirty_dpms)
				printf("DPMS off = %u\n", c->dpms);

			if (p->buf) {
				surface_buffer_put_fb(&p->surf.base, p->buf);
				dprintf("plane pending events %u -> %u\n",
					p->surf.pending_events, p->surf.pending_events - 1);
				p->surf.pending_events--;
				p->buf = NULL;
			}
			if (c->buf) {
				surface_buffer_put_fb(&c->surf.base, c->buf);
				dprintf("crtc pending events %u -> %u\n",
					c->surf.pending_events, c->surf.pending_events - 1);
				c->surf.pending_events--;
				c->buf = NULL;
			}
		}
		return;
	}
#endif

	for (i = 0; i < ctx->count_crtcs; i++) {
		struct my_plane *p = &ctx->planes[i];
		struct my_crtc *c = container_of(p->base.crtc, struct my_crtc, base);

#ifdef LEGACY_API
		if (p->old_fb_id) {
			struct buffer *buf = surface_find_buffer_by_fb_id(&p->surf.base, p->old_fb_id);
			if (buf)
				surface_buffer_put_fb(&p->surf.base, buf);
		}

		if (c->old_fb_id) {
			struct buffer *buf = surface_find_buffer_by_fb_id(&c->surf.base, c->old_fb_id);
			if (buf)
				surface_buffer_put_fb(&c->surf.base, buf);
		}

		p->old_fb_id = p->buf ? p->buf->fb_id : 0;
		c->old_fb_id = c->buf ? c->buf->fb_id : 0;
#endif

		p->dirty = false;
		p->buf = NULL;
		c->dirty = false;
		c->dirty_mode = false;
		c->dirty_dpms = false;
		c->cursor.dirty = false;
		c->buf = NULL;
	}
}

static float adjust_angle(struct my_plane *p)
{
	const float ang_adj = M_PI / 500.0f;

	p->state.ang += ang_adj;
	if (p->state.ang > 2.0f * M_PI)
		p->state.ang -= 2.0f * M_PI;

	return p->state.ang;
}

static float adjust_radius(struct my_plane *p)
{
	struct my_crtc *c = container_of(p->base.crtc, struct my_crtc, base);
	float rad_max = sqrtf(c->dispw * c->dispw + c->disph * c->disph) / 2.0f;
	float rad_min = -rad_max;
	float rad_adj = rad_max / 500.0f;

	p->state.rad += rad_adj * p->state.rad_dir;
	if (p->state.rad > rad_max && p->state.rad_dir > 0.0f) {
		p->state.rad_dir = -p->state.rad_dir;
	} else if (p->state.rad < rad_min && p->state.rad_dir < 0.0f) {
		p->state.rad_dir = -p->state.rad_dir;
	}

	return p->state.rad;
}

static int adjust_w(struct my_plane *p)
{
	struct my_crtc *c = container_of(p->base.crtc, struct my_crtc, base);
	int w_max = c->dispw;
	int w_adj = 1;//c->dispw / 100;

	p->state.w += w_adj * p->state.w_dir;
	if (p->state.w > w_max && p->state.w_dir > 0) {
		p->state.w_dir = -p->state.w_dir;
	} else if (p->state.w < 0 && p->state.w_dir < 0) {
		p->state.w = 0;
		p->state.w_dir = -p->state.w_dir;
	}

	return p->state.w;
}

static int adjust_h(struct my_plane *p)
{
	struct my_crtc *c = container_of(p->base.crtc, struct my_crtc, base);
	int h_max = c->disph;
	int h_adj = 1;//c->disph / 100;

	p->state.h += h_adj * p->state.h_dir;
	if (p->state.h > h_max && p->state.h_dir > 0) {
		p->state.h_dir = -p->state.h_dir;
	} else if (p->state.h < 0 && p->state.h_dir < 0) {
		p->state.h = 0;
		p->state.h_dir = -p->state.h_dir;
	}

	return p->state.h;
}

static void do_render(EGLDisplay dpy, EGLContext ctx, struct my_surface *surf, bool col, bool blur, bool blend)
{
	static const GLfloat verts[3][2] = {
		{ -1, -1, },
		{  1, -1, },
		{  0,  1, },
	};
	static const GLfloat colors[3][3] = {
		{ 1, 0, 0, },
		{ 0, 1, 0, },
		{ 0, 0, 1, },
	};
	GLfloat ar = (GLfloat) surf->base.width / (GLfloat) surf->base.height;

#if 0
	if (col) {
		surf->view_rotx += 0.25f;
		surf->view_roty += 1.0f;
	} else
		surf->view_rotz += 1.0f;

	if (!eglMakeCurrent(dpy, surf->egl_surface, surf->egl_surface, ctx))
		return;

	glViewport(0, 0, (GLint) surf->base.width, (GLint) surf->base.height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glFrustum(-ar, ar, -1, 1, 5.0, 60.0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0, 0.0, -10.0);

	if (col)
		glClearColor(0.8, 0.4, 0.4, 0.0);
	else
		glClearColor(0.4, 0.4, 0.4, 0.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glPushMatrix();
	glRotatef(surf->view_rotx, 1, 0, 0);
	glRotatef(surf->view_roty, 0, 1, 0);
	glRotatef(surf->view_rotz, 0, 0, 1);

	glVertexPointer(2, GL_FLOAT, 0, verts);
	glColorPointer(3, GL_FLOAT, 0, colors);
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);

	glDrawArrays(GL_TRIANGLES, 0, 3);

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);

	glPopMatrix();
#else
	if (render)
		gl_surf_render(dpy, ctx, surf, col, true, blur, blend);
	if (blank)
		gl_surf_clear(dpy, ctx, surf, false);
#endif
}

static void clear_rect(EGLDisplay dpy, EGLContext ctx, struct my_surface *surf,
		       int x, int y, int w, int h)
{
	glViewport(0, 0, (GLint) surf->base.width, (GLint) surf->base.height);
	glScissor(x, surf->base.height - y - h, w, h);
	glEnable(GL_SCISSOR_TEST);
	glClearColor(1.0, 0.0, 1.0, 0.0);
	glClear(GL_COLOR_BUFFER_BIT);
	glDisable(GL_SCISSOR_TEST);
}

static void swap_buffers(EGLDisplay dpy, struct my_surface *surf)
{
	//glFlush();
	eglSwapBuffers(dpy, surf->egl_surface);
}

static const EGLint attribs[] = {
	EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
	EGL_RED_SIZE, 1,
	EGL_GREEN_SIZE, 1,
	EGL_BLUE_SIZE, 1,
	EGL_ALPHA_SIZE, 0,
	EGL_DEPTH_SIZE, 1,
	EGL_RENDERABLE_TYPE, EGL_OPENGL_BIT,
	EGL_NONE
};

static bool my_surface_alloc(struct my_surface *s,
			     struct gbm_device *gbm,
			     unsigned int fmt,
			     unsigned int w,
			     unsigned int h,
			     EGLDisplay dpy)
{
	EGLint num_configs = 0;
	EGLConfig config;

	if (!eglChooseConfig(dpy, attribs, &config, 1, &num_configs) || num_configs != 1)
		return false;

	if (!surface_alloc(&s->base, gbm, fmt, w, h))
		return false;

	if (!gl_surf_init(dpy, config, s))
		return false;

	return true;
}

static bool produce_frame(EGLDisplay dpy, EGLContext ctx, struct my_plane *p)
{
	struct my_crtc *c = container_of(p->base.crtc, struct my_crtc, base);

	if (get_free_buffer(&c->surf) < 0 || get_free_buffer(&p->surf) < 0)
		return false;

	do_render(dpy, ctx, &c->surf, false, blur, false);
	if (!c->onoff_phase && anim_clear)
		clear_rect(dpy, ctx, &c->surf,
			   p->dst.x1,
			   p->dst.y1,
			   p->dst.x2 - p->dst.x1,
			   p->dst.y2 - p->dst.y1);
	swap_buffers(dpy, &c->surf);
	do_render(dpy, ctx, &p->surf, false/*true*/, blur, true);
	swap_buffers(dpy, &p->surf);

	return true;
}

static bool handle_crtc(struct my_ctx *my_ctx,
			struct gbm_device *gbm,
			EGLDisplay dpy,
			EGLContext ctx,
			const char *mode_name,
			const char *mode_name_2,
			struct my_crtc *c, struct my_plane *p)
{
	if (!pick_mode(&c->base, &c->mode[0], mode_name))
		return false;

	if (!mode_name_2 || !pick_mode(&c->base, &c->mode[1], mode_name_2))
		c->mode[1] = c->mode[0];

	c->dirty_mode = true;

	if (0) {
#if 0
		snprintf(c->mode.name, sizeof c->mode.name, "1920x1080");
		c->mode.vrefresh = 60;
		c->mode.clock = 148500;
		c->mode.hdisplay = 1920;
		c->mode.hsync_start = 2008;
		c->mode.hsync_end = 2052;
		c->mode.htotal = 2200;
		c->mode.vdisplay = 1080;
		c->mode.vsync_start = 1084;
		c->mode.vsync_end = 1089;
		c->mode.vtotal = 1125;
		c->mode.flags = DRM_MODE_FLAG_NHSYNC | DRM_MODE_FLAG_NVSYNC;
#endif
#if 0
		snprintf(c->mode.name, sizeof c->mode.name, "1024x768");
		c->mode.vrefresh = 60;
		c->mode.clock = 65000;
		c->mode.hdisplay = 1024;
		c->mode.hsync_start = 1048;
		c->mode.hsync_end = 1184;
		c->mode.htotal = 1344;
		c->mode.vdisplay = 768;
		c->mode.vsync_start = 771;
		c->mode.vsync_end = 777;
		c->mode.vtotal = 806;
		c->mode.flags = 0xa;
#endif
#if 0
		snprintf(c->mode.name, sizeof c->mode.name, "1280x720");
		c->mode.vrefresh = 60;
		c->mode.clock = 74250;
		c->mode.hdisplay = 1280;
		c->mode.hsync_start = 1390;
		c->mode.hsync_end = 1430;
		c->mode.htotal = 1650;
		c->mode.vdisplay = 720;
		c->mode.vsync_start = 725;
		c->mode.vsync_end = 730;
		c->mode.vtotal = 750;
		c->mode.flags = DRM_MODE_FLAG_PHSYNC | DRM_MODE_FLAG_PVSYNC;
#endif
#if 0
		snprintf(c->mode.name, sizeof c->mode.name, "1366x768");
		c->mode.vrefresh = 60;
		c->mode.clock = 85885;
		c->mode.hdisplay = 1366;
		c->mode.hsync_start = 1439;
		c->mode.hsync_end = 1583;
		c->mode.htotal = 1800;
		c->mode.vdisplay = 768;
		c->mode.vsync_start = 769;
		c->mode.vsync_end = 772;
		c->mode.vtotal = 795;
		c->mode.flags = 6;
#endif
#if 0
		snprintf(c->mode.name, sizeof c->mode.name, "720x576");
		c->mode.vrefresh = 50;
		c->mode.clock = 27000;
		c->mode.hdisplay = 720;
		c->mode.hsync_start = 732;
		c->mode.hsync_end = 796;
		c->mode.htotal = 864;
		c->mode.vdisplay = 576;
		c->mode.vsync_start = 581;
		c->mode.vsync_end = 586;
		c->mode.vtotal = 625;
		c->mode.flags = 0xa;
#endif
#if 0
		snprintf(c->mode.name, sizeof c->mode.name, "1920x1080i");
		c->mode.vrefresh = 60;
		c->mode.clock = 74250;
		c->mode.hdisplay = 1920;
		c->mode.hsync_start = 2008;
		c->mode.hsync_end = 2052;
		c->mode.htotal = 2200;
		c->mode.vdisplay = 1080;
		c->mode.vsync_start = 1084;
		c->mode.vsync_end = 1094;
		c->mode.vtotal = 1125;
		c->mode.flags = 0x15;
#endif
		if (!pick_mode(&c->base, &c->mode[0], mode_name))
			return false;

		c->mode[1] = c->mode[0];

		c->dirty_mode = true;
	}
	printf("orig mode = %s, modes = %s / %s\n", c->original_mode.name,
	       c->mode[0].name, c->mode[1].name);

	/* FIXME need to dig out the mode struct for c->mode_id instead */
	c->dispw = c->mode[c->mode_idx].hdisplay;
	c->disph = c->mode[c->mode_idx].vdisplay;

	if (!my_surface_alloc(&p->surf, gbm, DRM_FORMAT_XRGB8888, 960, 576, dpy))
		return false;

#if 1
	p->src.x1 = 0 << 16;
	p->src.y1 = 0 << 16;
	p->src.x2 = p->surf.base.width << 16;
	p->src.y2 = p->surf.base.height << 16;
#else
	p->src.x1 = (p->surf.base.width*1/3) << 16;
	p->src.y1 = (p->surf.base.height*1/3) << 16;
	p->src.x2 = (p->surf.base.width*3/3) << 16;
	p->src.y2 = (p->surf.base.height*3/3) << 16;
#endif

	p->dst.x1 = 0;
	p->dst.x2 = c->dispw/2;
	p->dst.y1 = 0;
	p->dst.y2 = c->disph/2;

	if (!my_surface_alloc(&c->surf, gbm, DRM_FORMAT_XRGB8888, c->dispw, c->disph, dpy))
		return false;

	{
		uint32_t buf[64][64];
		int i, j;

		if (!bo_alloc(&c->cursor.bo, gbm, DRM_FORMAT_ARGB8888, 64, 64))
			return false;

		for (i = 0; i < 64; i++) {
			for (j = 0; j < 64; j++) {
				uint32_t a = (j + i) & 255;
				uint32_t p = (j * i) & 255;
				buf[i][j] = (a << 24) | (p << 24) | (p << 16) | p;
			}
		}

		gbm_bo_write(c->cursor.bo.bo, buf, sizeof buf);
	}

	c->dirty = true;
	p->dirty = true;
	c->cursor.dirty = true;

	if (produce_frame(dpy, ctx, p))
		plane_commit(my_ctx, p);

	return true;
}

static bool animate_crtc(struct my_ctx *my_ctx,
			 EGLDisplay dpy,
			 EGLContext ctx,
			 struct my_crtc *c, struct my_plane *p)
{
	int w, h, x, y;

	if (get_free_buffer(&c->surf) < 0 || get_free_buffer(&p->surf) < 0)
		return false;

	switch (anim_mode) {
		float rad, ang;
	case ANIM_CURVE:
		rad = adjust_radius(p);
		ang = adjust_angle(p);
		w = adjust_w(p);
		h = adjust_h(p);
#if 0
		if (w < 4)
			w = 4;
		if (h < 4)
			h = 4;
#endif
		x = rad * sinf(ang) + c->dispw / 2 - w/2;
		y = rad * cosf(ang) + c->disph / 2 - h/2;
		break;
	case ANIM_RAND:
		w = rand() % (c->dispw/2);
		h = rand() % (c->disph/2);
#if 0
		if (w < 4)
			w = 4;
		if (h < 4)
			h = 4;
#endif
		x = rand() % (c->dispw-16) + 8 - w/2;
		y = rand() % (c->disph-16) + 8 - h/2;
		break;
	case ANIM_STATIC:
		w = p->dst.x2 - p->dst.x1;
		h = p->dst.y2 - p->dst.y1;
		x = p->dst.x1;
		y = p->dst.y1;
		break;
	}

	p->dst.x1 = x;
	p->dst.y1 = y;
	p->dst.x2 = p->dst.x1 + w;
	p->dst.y2 = p->dst.y1 + h;
	p->dirty = true;

	c->dirty = true;

	if (toggle_onoff && c->toggle_onoff_cntr++ > 3) {
		c->onoff_phase = !c->onoff_phase;
		plane_enable(p, !c->onoff_phase);
		c->toggle_onoff_cntr = 0;
	}
	produce_frame(dpy, ctx, p);
	plane_commit(my_ctx, p);

	c->frames++;

	if (1) {
		struct timespec cur;
		clock_gettime(CLOCK_MONOTONIC, &cur);
		struct timespec diff = cur;
		tp_sub(&diff, &c->prev);
		float secs = (float) diff.tv_sec + diff.tv_nsec / 1000000000.0f;
		if (secs >= 5.0f) {
			printf("crtc [%d] id = %u: %u frames in %f secs, %f fps\n",
			       c->base.crtc_idx, c->base.crtc_id, c->frames, secs, c->frames / secs);
			c->prev = cur;
			c->frames = 0;
		}
	}

	return true;
}

static void cmd_help(void)
{
	printf("commands:\n"
	       " o = enable/disable overlay plane\n"
	       " O = enable/disable overlay on/off toggling\n"
	       " s/x/z/c = move overlay plane\n"
	       " S/X/Z/C = resize overlay plane\n"
	       " t = toggle test mode\n"
	       " T = toggle throttling mode\n"
	       " b = toggle blurred rendering\n"
	       " B = toggle solid color rendering\n"
	       " R = toggle rendering\n"
	       " r = toggle black rect under overlay rendering\n"
	       " a = toggle animation mode static/spiral/random\n"
	       " d = commit w/o rendering once\n"
	       " D = toggle DPMS on<->off\n"
	       " m = toggle display mode between two options\n"
	       " u = enable/disable cursor plane\n");
}

static void usage(const char *name)
{
	fprintf(stderr, "Usage: %s <connector> <mode> <mode> [[<connector> <mode> <mode>] ...]\n",
		name);
}

int main(int argc, char *argv[])
{
	struct my_ctx my_ctx = {};
	struct my_crtc c[8] = {
		[0] = {
		},
	};
	struct my_plane p[8] = {
		[0] = {
			.state = {
				.ang = 0.0f,
				.rad_dir = 1.0f,
				.rad = 0.0f,
				.w_dir = 1,
				.w = 0,
				.h_dir = 1,
				.h = 0,
			},
		},
	};
	struct ctx uctx = {};
	int fd;
	bool rotate = false;
	bool enable = true;
	bool quit = false;
	int r;
	int i;
	struct gbm_device *gbm;
	drmEventContext evtctx = {
		.version = DRM_EVENT_CONTEXT_VERSION,
#ifndef LEGACY_API
		.atomic_handler = atomic_event,
#endif
	};
	EGLDisplay dpy;
	EGLContext ctx;
	EGLint major, minor;
	EGLint num_configs = 0;
	EGLConfig config;
	int count_crtcs = 0;
	const char *modes[2][8] = {};

	if (argc < 3) {
		usage(argv[0]);
		return 1;
	}

	fd = drmOpen("i915", NULL);
	if (fd < 0)
		return 2;

	if (!init_ctx(&uctx, fd))
		return 3;

	for (i = 0; i < argc - 3; i += 3) {
		if (count_crtcs) {
			c[count_crtcs] = c[0];
			p[count_crtcs] = p[0];
		}

		init_crtc(&c[count_crtcs].base, &uctx);
		init_plane(&p[count_crtcs].base, &c[count_crtcs].base, &uctx);

		if (pick_connector(&c[count_crtcs].base, argv[i + 1]) &&
		    pick_encoder(&c[count_crtcs].base) &&
		    pick_crtc(&c[count_crtcs].base) &&
		    pick_plane(&p[count_crtcs].base)) {
			modes[0][count_crtcs] = argv[i + 2];
			modes[1][count_crtcs] = argv[i + 3];
			count_crtcs++;
			continue;
		}

		release_plane(&p[count_crtcs].base);
		release_crtc(&c[count_crtcs].base);
		release_encoder(&c[count_crtcs].base);
		release_connector(&c[count_crtcs].base);
	}

	if (count_crtcs == 0) {
		usage(argv[0]);
		return 4;
	}

	gbm = gbm_create_device(fd);
	if (!gbm)
		return 5;

	dpy = eglGetDisplay(gbm);
	if (dpy == EGL_NO_DISPLAY)
		return 6;

	if (!eglInitialize(dpy, &major, &minor))
		return 7;

	eglBindAPI(EGL_OPENGL_API);

	if (!eglChooseConfig(dpy, attribs, &config, 1, &num_configs) || num_configs != 1)
		return 8;

	ctx = eglCreateContext(dpy, config, EGL_NO_CONTEXT, NULL);
	if (!ctx)
		return 9;

	eglMakeCurrent(dpy, EGL_NO_SURFACE, EGL_NO_SURFACE, ctx);

	gl_init();

	my_ctx.fd = fd;
	my_ctx.planes = p;
	my_ctx.count_crtcs = count_crtcs;

	for (i = 0; i < count_crtcs; i++) {
		populate_crtc_props(fd, &c[i]);
		populate_connector_props(fd, &c[i]);
		populate_plane_props(fd, &p[i]);
		plane_enable(&p[i], enable);
		if (!handle_crtc(&my_ctx, gbm, dpy, ctx, modes[0][i], modes[1][i], &c[i], &p[i]))
			return 10;
	}
	commit_state(&my_ctx);

	printf("pre\n");
	for (i = 0; i < count_crtcs; i++) {
		plane_enable(&p[i], !enable);
		plane_enable(&p[i], enable);
	}
	commit_state(&my_ctx);
	printf("mid\n");
	for (i = 0; i < count_crtcs; i++) {
		plane_enable(&p[i], !enable);
		plane_enable(&p[i], enable);
	}
	commit_state(&my_ctx);
	printf("post\n");

	term_init();

	srand(time(NULL));

	bool test_running = false;
	struct timespec prev;

	struct timeval timeout;
	struct timeval *t = NULL;

	cmd_help();

	while (!quit) {
		char cmd;
		fd_set fds;
		int maxfd;

		FD_ZERO(&fds);

		maxfd = STDIN_FILENO;
		FD_SET(STDIN_FILENO, &fds);

		maxfd = max(maxfd, fd);
		FD_SET(fd, &fds);

		if (t) {
			t->tv_sec = 0;
			t->tv_usec = 0;
		}

		r = select(maxfd + 1, &fds, NULL, NULL, t);

		if (r < 0 && errno == EINTR)
			continue;

		if (FD_ISSET(fd, &fds)) {
			if (test_running)
				t = &timeout;
			drmHandleEvent(fd, &evtctx);
		}

		if (t && test_running) {
			bool no_sleep = false;

			/*
			 * sleep if everyone is waiting for
			 * free buffers, otherwise don't.
			 */
			for (i = 0; i < count_crtcs; i++)
				no_sleep |= animate_crtc(&my_ctx, dpy, ctx, &c[i], &p[i]);
			commit_state(&my_ctx);

			if (no_sleep)
				t = &timeout;
			else
				t = NULL;
		}

		if (!FD_ISSET(STDIN_FILENO, &fds))
			continue;

		if (read(0, &cmd, 1) < 0)
			break;

		switch (cmd) {
		case ' ':
			rotate = !rotate;
			for (i = 0; i < count_crtcs; i++) {
				c[i].dirty = true;
				p[i].rotate = rotate;
				p[i].dirty = true;
				if (produce_frame(dpy, ctx, &p[i]))
					plane_commit(&my_ctx, &p[i]);
			}
			commit_state(&my_ctx);
			printf("overlay plane rotation = %d\n", rotate ? 180 : 0);
			break;
		case '+':
		case '-':
			if (cmd == '+' && setplane_delay < 20000)
				setplane_delay += 100;
			if (cmd == '-' && setplane_delay)
				setplane_delay -= 100;
			printf("setplane delay is %u usec\n", setplane_delay);
			break;
		case 'o':
			enable = !enable;
			for (i = 0; i < count_crtcs; i++) {
				c[i].dirty = true;
				p[i].dirty = true;
				plane_enable(&p[i], enable);
				if (produce_frame(dpy, ctx, &p[i]))
					plane_commit(&my_ctx, &p[i]);
			}
			commit_state(&my_ctx);
			printf("overlay plane enabled = %s\n",
			       enable ? "yes" : "no");
			break;
		case 'O':
			toggle_onoff = !toggle_onoff;
			for (i = 0; i < count_crtcs; i++)
				c[i].onoff_phase = false;
			printf("overlay on/off toggling enabled = %s\n",
			       toggle_onoff ? "yes" : "no");
			break;
		case 'q':
			quit = 1;
			break;
		case 's':
		case 'x':
			for (i = 0; i < count_crtcs; i++) {
				p[i].dst.y1 += (cmd == 's') ? -1 : 1;
				p[i].dst.y2 += (cmd == 's') ? -1 : 1;
				p[i].dirty = true;
				c[i].dirty = true;
				if (produce_frame(dpy, ctx, &p[i]))
					plane_commit(&my_ctx, &p[i]);
			}
			commit_state(&my_ctx);
			break;
		case 'S':
		case 'X':
			for (i = 0; i < count_crtcs; i++) {
				p[i].dst.y2 += (cmd == 'S') ? -1 : 1;
				p[i].dirty = true;
				c[i].dirty = true;
				if (produce_frame(dpy, ctx, &p[i]))
					plane_commit(&my_ctx, &p[i]);
			}
			commit_state(&my_ctx);
			break;
		case 'z':
		case 'c':
			for (i = 0; i < count_crtcs; i++) {
				p[i].dst.x1 += (cmd == 'z') ? -1 : 1;
				p[i].dst.x2 += (cmd == 'z') ? -1 : 1;
				p[i].dirty = true;
				c[i].dirty = true;
				if (produce_frame(dpy, ctx, &p[i]))
					plane_commit(&my_ctx, &p[i]);
			}
			commit_state(&my_ctx);
			break;
		case 'Z':
		case 'C':
			for (i = 0; i < count_crtcs; i++) {
				p[i].dst.x2 += (cmd == 'Z') ? -1 : 1;
				p[i].dirty = true;
				c[i].dirty = true;
				if (produce_frame(dpy, ctx, &p[i]))
					plane_commit(&my_ctx, &p[i]);
			}
			commit_state(&my_ctx);
			break;
		case 't':
			test_running = !test_running;
			t = &timeout;
			if (test_running) {
				clock_gettime(CLOCK_MONOTONIC, &prev);
				for (i = 0; i < count_crtcs; i++) {
					c[i].prev = prev;
					c[i].frames = 0;
				}
			}
			printf("test running = %s\n",
			       test_running ? "yes" : "no");
			break;
		case 'T':
			throttle = !throttle;
			printf("throttle to vrefresh = %s\n",
			       throttle ? "yes" : "no");
			break;
		case 'b':
			blur = !blur;
			printf("render blur = %s\n",
			       blur ? "yes" : "no");
			break;
		case 'B':
			blank = !blank;
			printf("render solid color = %s\n",
			       blank ? "yes" : "no");
			break;
		case 'R':
			render = !render;
			printf("render = %s\n",
			       render ? "yes" : "no");
			break;
		case 'a':
			anim_mode = (anim_mode + 1) % ANIM_COUNT;
			printf("animation mode = %s\n",
			       anim_mode == ANIM_CURVE ? "spiral" :
			       anim_mode == ANIM_RAND ? "random" :
			       anim_mode == ANIM_STATIC ? "static" :
			       "?");
			break;
		case 'r':
			anim_clear = !anim_clear;
			printf("render black rect under overlay plane = %s\n",
			       anim_clear ? "yes" : "no");
			break;
		case 'd':
			for (i = 0; i < count_crtcs; i++) {
				p[i].dirty = true;
				c[i].dirty = true;
				plane_commit(&my_ctx, &p[i]);
			}
			commit_state(&my_ctx);
			break;
		case 'D':
			for (i = 0; i < count_crtcs; i++) {
				c[i].dpms = !c[i].dpms;
				c[i].dirty_dpms = true;
				plane_commit(&my_ctx, &p[i]);
			}
			commit_state(&my_ctx);
			break;
		case 'm':
			for (i = 0; i < count_crtcs; i++) {
				c[i].mode_idx = !c[i].mode_idx;
				c[i].dirty_mode = true;
				if (produce_frame(dpy, ctx, &p[i]))
					plane_commit(&my_ctx, &p[i]);
			}
			commit_state(&my_ctx);
			break;
		case 'u':
			for (i = 0; i < count_crtcs; i++) {
				c[i].cursor.id = c[i].cursor.id ? 0 : bo_handle(&c[i].cursor.bo);
				printf("cursor to %u\n", c[i].cursor.id);
				c[i].cursor.dirty = true;
				plane_commit(&my_ctx, &p[i]);
			}
			commit_state(&my_ctx);
			break;
		case 'h':
			cmd_help();
			break;
		}
	}

	term_deinit();

	for (i = 0; i < count_crtcs; i++) {
		c[i].dirty = true;
		c[i].mode[c->mode_idx] = c[i].original_mode;
		c[i].dirty_mode = true;

		plane_enable(&p[i], false);
		plane_commit(&my_ctx, &p[i]);

	}
	commit_state(&my_ctx);

	glUseProgram(0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindTexture(GL_TEXTURE_2D, 0);
	eglMakeCurrent(dpy, EGL_NO_SURFACE, EGL_NO_SURFACE, ctx);

	gl_fini();

	for (i = 0; i < count_crtcs; i++) {
#if 0
		/* FIXME assert() failure in buffer ref count handling */
		gl_surf_fini(dpy, &c[i].surf);
		gl_surf_fini(dpy, &p[i].surf);
#endif
		surface_free(&c[i].surf.base);
		surface_free(&p[i].surf.base);
	}

	eglDestroyContext(dpy, ctx);
	eglTerminate(dpy);

	gbm_device_destroy(gbm);

	free_ctx(&uctx);

	drmClose(fd);

	return 0;
}
